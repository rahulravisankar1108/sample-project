const express = require("express");
const mongoose = require("mongoose");

const CourseModel = mongoose.model("Course")
const router = express.Router();

router.get("/add",(req, res)=>{
    res.render("add-course")
})

router.post("/add",(req, res)=>{
    var course = new CourseModel();
    course.courseName=req.body.courseName;
    course.courseDuration=req.body.courseDuration;
    course.courseFee=req.body.courseFee;
    course.courseId=Math.ceil(Math.random()*100000000);

    course.save((err, doc)=>{
        if(!err) {
            res.redirect("/course/list")
            res.json({message : "successful", status : "ok"})
        }
        else {
            res.send("Error occured");
        }
    });

})


router.get("/list",(req, res)=>{

    // getting
    CourseModel.find((err,docs)=>{
        if(!err) {
            // console.log(docs);
            res.render('list',{data : docs})
        }
        else
        {
            res.send('Error');
        }

    });
    res.send("Course Controller");
});

module.exports = router;